/*
 * Copyright (C) 2005 Red Hat, Inc.
 * Copyright (C) 2001 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 *      Havoc Pennington <hp@redhat.com>
 */

#include <config.h>

#include "gsm-service.h"

#include <signal.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <glib/gi18n.h>

#include "gsm-startup-monitor.h"
#include "gsm-dialogs.h"
#include "gsm-gconf.h"
#include "gsm-debug.h"

struct _GsmServicePrivate
{
  char *id;
  char *path;
  char *name;
  char *description;
  char *icon_name;
  char *exec;
  char *enabled_key;
  char *disabled_key;

  guint toggle_key_notify;

  GsmStartupMonitor *startup_monitor;
  gulong startup_completed_handler;

  GPid pid;
  guint child_watch;

  SnLauncherContext *sn_context;
  guint sn_timeout;

  int respawn_count;
  GTime last_respawn_time;

  GtkWidget *error_dialog;

  guint enabled : 1;
};

#define GSM_SERVICE_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), GSM_TYPE_SERVICE, GsmServicePrivate))

#define GSM_SERVICE_GROUP                "Desktop Service"
#define GSM_SERVICE_SN_TIMEOUT           (30 * 1000)
#define GSM_SERVICE_MAX_RESPAWNS         5
#define GSM_SERVICE_MAX_RESPAWNS_TIMEOUT (10 * 1000)

enum
{
  PROP_0,
  PROP_ID,
  PROP_PATH,
  PROP_NAME,
  PROP_DESCRIPTION,
  PROP_ICON_NAME,
  PROP_EXEC,
  PROP_ENABLED,
  PROP_ENABLED_KEY,
  PROP_DISABLED_KEY
};

enum
{
  STARTUP_COMPLETED,
  LAST_SIGNAL
};

G_DEFINE_TYPE (GsmService, gsm_service, G_TYPE_OBJECT);

static guint signals[LAST_SIGNAL] = { 0 };

static void
gsm_service_finalize (GObject *object)
{
  GsmService *service = GSM_SERVICE (object);

  gsm_debug (SERVICES, "Finalizing service '%s' (%p)\n",
	     service->priv->id, service);

  gsm_service_stop (service);

  if (service->priv->toggle_key_notify != 0)
    {
      GConfClient *client;

      client = gsm_gconf_get_client ();

      gconf_client_notify_remove (client, service->priv->toggle_key_notify);
      service->priv->toggle_key_notify = 0;
    }

  g_free (service->priv->id);
  service->priv->id = NULL;

  g_free (service->priv->path);
  service->priv->path = NULL;

  g_free (service->priv->name);
  service->priv->name = NULL;

  g_free (service->priv->description);
  service->priv->description = NULL;

  g_free (service->priv->icon_name);
  service->priv->icon_name = NULL;

  g_free (service->priv->exec);
  service->priv->exec = NULL;

  g_free (service->priv->enabled_key);
  service->priv->enabled_key = NULL;

  g_free (service->priv->disabled_key);
  service->priv->disabled_key = NULL;

  service->priv = NULL;

  G_OBJECT_CLASS (gsm_service_parent_class)->finalize (object);
}

static void
gsm_service_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  GsmService *service = GSM_SERVICE (object);

  switch (prop_id)
    {
    case PROP_ID:
      gsm_service_set_id (service, g_value_get_string (value));
      break;

    case PROP_PATH:
      gsm_service_set_path (service, g_value_get_string (value));
      break;

    case PROP_NAME:
      gsm_service_set_name (service, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      gsm_service_set_description (service, g_value_get_string (value));
      break;

    case PROP_ICON_NAME:
      gsm_service_set_icon_name (service, g_value_get_string (value));
      break;

    case PROP_EXEC:
      gsm_service_set_exec (service, g_value_get_string (value));
      break;

    case PROP_ENABLED_KEY:
      gsm_service_set_enabled_key (service, g_value_get_string (value));
      break;

    case PROP_DISABLED_KEY:
      gsm_service_set_disabled_key (service, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gsm_service_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  GsmService *service = GSM_SERVICE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, service->priv->id);
      break;

    case PROP_PATH:
      g_value_set_string (value, service->priv->path);
      break;

    case PROP_NAME:
      g_value_set_string (value, service->priv->name);
      break;

    case PROP_DESCRIPTION:
      g_value_set_string (value, service->priv->description);
      break;

    case PROP_ICON_NAME:
      g_value_set_string (value, service->priv->icon_name);
      break;

    case PROP_EXEC:
      g_value_set_string (value, service->priv->exec);
      break;

    case PROP_ENABLED:
      g_value_set_boolean (value, service->priv->enabled);
      break;

    case PROP_ENABLED_KEY:
      g_value_set_string (value, service->priv->enabled_key);
      break;

    case PROP_DISABLED_KEY:
      g_value_set_string (value, service->priv->disabled_key);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gsm_service_class_init (GsmServiceClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  gobject_class->finalize     = gsm_service_finalize;
  gobject_class->set_property = gsm_service_set_property;
  gobject_class->get_property = gsm_service_get_property;

  g_object_class_install_property (gobject_class,
                                   PROP_ID,
                                   g_param_spec_string ("id",
                                                        "ID",
                                                        "The service's identifier",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_PATH,
                                   g_param_spec_string ("path",
                                                        "Path",
                                                        "The path to the service's .desktop-service file",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_NAME,
                                   g_param_spec_string ("name",
                                                        "Name",
                                                        "The service's name",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_DESCRIPTION,
                                   g_param_spec_string ("description",
                                                        "Description",
                                                        "A description of the service",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_ICON_NAME,
                                   g_param_spec_string ("icon-name",
                                                        "Icon Name",
                                                        "An icon name for the service",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_EXEC,
                                   g_param_spec_string ("exec",
                                                        "Exec",
                                                        "The command line used to launch the service",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_ENABLED,
                                   g_param_spec_boolean ("enabled",
							 "Enabled",
							 "Whether the service is currently enabled",
							 TRUE,
							 G_PARAM_READABLE));

  g_object_class_install_property (gobject_class,
                                   PROP_ENABLED_KEY,
                                   g_param_spec_string ("enabled-key",
                                                        "Enabled Key",
                                                        "GConf key which controls whether the service is enabled",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  g_object_class_install_property (gobject_class,
                                   PROP_DISABLED_KEY,
                                   g_param_spec_string ("disabled-key",
                                                        "Disabled Key",
                                                        "GConf key which controls whether the service is disabled",
                                                        NULL,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

  signals[STARTUP_COMPLETED] =
    g_signal_new ("startup-completed",
		  G_OBJECT_CLASS_TYPE (gobject_class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GsmServiceClass, startup_completed),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

  g_type_class_add_private (gobject_class, sizeof (GsmServicePrivate));
}

static void
gsm_service_init (GsmService *service)
{
  service->priv = GSM_SERVICE_GET_PRIVATE (service);

  service->priv->enabled = TRUE;
}

static char *
get_gconf_toggle_key (GKeyFile   *key_file,
		      const char *key_file_path,
		      const char *key)
{
  char *toggle_key;

  if ((toggle_key = g_key_file_get_string (key_file, GSM_SERVICE_GROUP, key, NULL)) != NULL)
    {
      char *why_invalid;

      why_invalid = NULL;
      if (!gconf_valid_key (toggle_key, &why_invalid))
	{
	  gsm_warning ("'%s' contains an '%s' with an invalid value '%s': %s",
		       key_file_path, key, toggle_key, why_invalid);
	  g_free (why_invalid);
	  g_free (toggle_key);
	  toggle_key = NULL;
	}
    }

  return toggle_key;
}

static void
get_gconf_toggle_keys (GKeyFile    *key_file,
		       const char  *key_file_path,
		       char       **enabled_key_ret,
		       char       **disabled_key_ret)
{
  char *enabled_key;

  if ((enabled_key = get_gconf_toggle_key (key_file, key_file_path, "EnabledKey")) != NULL)
    {
      if (g_key_file_has_key (key_file, GSM_SERVICE_GROUP, "DisabledKey", NULL))
	gsm_warning ("'%s' contains an 'EnabledKey' and a 'DisabledKey' value; the 'DisabledKey' value will be ignored",
		     key_file_path);

      *enabled_key_ret = enabled_key;
    }
  else
    {
      *disabled_key_ret = get_gconf_toggle_key (key_file, key_file_path, "DisabledKey");
    }
}

GsmService *
gsm_service_new (const char *id,
		 const char *path)
{
  GsmService *retval;
  GKeyFile   *key_file;
  GError     *error;
  char       *name;
  char       *description;
  char       *icon_name;
  char       *exec;
  char       *enabled_key;
  char       *disabled_key;

  g_return_val_if_fail (id != NULL, NULL);
  g_return_val_if_fail (path != NULL, NULL);

  gsm_debug (SERVICES, "Loading service '%s' from '%s'\n", id, path);

  key_file = g_key_file_new ();

  error = NULL;
  if (!g_key_file_load_from_file (key_file, path, 0, &error))
    {
      gsm_warning ("Failed to load service '%s' from '%s': %s",
		   id, path, error->message);
      g_error_free (error);
      g_key_file_free (key_file);
      return NULL;
    }

  if (!g_key_file_has_key (key_file, GSM_SERVICE_GROUP, "Name", NULL))
    {
      gsm_warning ("'%s' contains no 'Name' key", path);
      g_key_file_free (key_file);
      return NULL;
    }

  if (!g_key_file_has_key (key_file, GSM_SERVICE_GROUP, "Exec", NULL))
    {
      gsm_warning ("'%s' contains no 'Exec' key", path);
      g_key_file_free (key_file);
      return NULL;
    }

#define GET_LOCALE_STRING(n) g_key_file_get_locale_string (key_file, GSM_SERVICE_GROUP, (n), NULL, NULL)

  if ((name = GET_LOCALE_STRING ("Name")) == NULL)
    {
      gsm_warning ("'%s' contains an invalid 'Name' key", path);
      g_key_file_free (key_file);
      return NULL;
    }

  description = GET_LOCALE_STRING ("Description");
  icon_name   = GET_LOCALE_STRING ("Icon");

#undef GET_LOCALE_STRING

  if ((exec = g_key_file_get_string (key_file, GSM_SERVICE_GROUP, "Exec", NULL)) == NULL)
    {
      gsm_warning ("'%s' contains an invalid 'Exec' key", path);
      g_free (name);
      g_free (description);
      g_free (icon_name);
      g_key_file_free (key_file);
      return NULL;
    }

  enabled_key = FALSE;
  disabled_key = FALSE;
  get_gconf_toggle_keys (key_file, path, &enabled_key, &disabled_key);

  retval = g_object_new (GSM_TYPE_SERVICE,
			 "id",           id,
			 "path",         path,
			 "name",         name,
			 "description",  description,
			 "icon-name",    icon_name,
			 "exec",         exec,
			 "enabled-key",  enabled_key,
			 "disabled-key", disabled_key,
			 NULL);

  g_free (disabled_key);
  g_free (enabled_key);
  g_free (exec);
  g_free (icon_name);
  g_free (description);
  g_free (name);

  g_key_file_free (key_file);

  return retval;
}

const char *
gsm_service_get_id (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->id;
}

void
gsm_service_set_id (GsmService *service,
		    const char *id)
{
  g_return_if_fail (GSM_IS_SERVICE (service));
  g_return_if_fail (id != NULL);

  g_free (service->priv->id);
  service->priv->id = g_strdup (id);

  gsm_debug (SERVICES, "Set service %p id to '%s'\n",
	     service, service->priv->id);

  g_object_notify (G_OBJECT (service), "id");
}

const char *
gsm_service_get_path (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->path;
}

void
gsm_service_set_path (GsmService *service,
		      const char *path)
{
  g_return_if_fail (GSM_IS_SERVICE (service));
  g_return_if_fail (path != NULL);

  g_free (service->priv->path);
  service->priv->path = g_strdup (path);

  gsm_debug (SERVICES, "Set service '%s' (%p) path to '%s'\n",
	     service->priv->id, service, service->priv->path);

  g_object_notify (G_OBJECT (service), "path");
}

const char *
gsm_service_get_name (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->name;
}

void
gsm_service_set_name (GsmService *service,
		      const char *name)
{
  g_return_if_fail (GSM_IS_SERVICE (service));
  g_return_if_fail (name != NULL);

  g_free (service->priv->name);
  service->priv->name = g_strdup (name);

  gsm_debug (SERVICES, "Set service '%s' (%p) name to '%s'\n",
	     service->priv->id, service, service->priv->name);

  g_object_notify (G_OBJECT (service), "name");
}

const char *
gsm_service_get_description (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->description;
}

void
gsm_service_set_description (GsmService *service,
			     const char *description)
{
  g_return_if_fail (GSM_IS_SERVICE (service));

  g_free (service->priv->description);
  service->priv->description = g_strdup (description);

  gsm_debug (SERVICES, "Set service '%s' (%p) description to '%s'\n",
	     service->priv->id, service,
	     service->priv->description ? service->priv->description : "(null)");

  g_object_notify (G_OBJECT (service), "description");
}

const char *
gsm_service_get_icon_name (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->icon_name;
}

void
gsm_service_set_icon_name (GsmService *service,
			   const char *icon_name)
{
  g_return_if_fail (GSM_IS_SERVICE (service));

  g_free (service->priv->icon_name);
  service->priv->icon_name = g_strdup (icon_name);

  gsm_debug (SERVICES, "Set service '%s' (%p) icon name to '%s'\n",
	     service->priv->id, service,
	     service->priv->icon_name ? service->priv->icon_name : "(null)");

  g_object_notify (G_OBJECT (service), "icon-name");
}

const char *
gsm_service_get_exec (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->exec;
}

void
gsm_service_set_exec (GsmService *service,
		      const char *exec)
{
  g_return_if_fail (GSM_IS_SERVICE (service));
  g_return_if_fail (exec != NULL);

  g_free (service->priv->exec);
  service->priv->exec = g_strdup (exec);

  gsm_debug (SERVICES, "Set service '%s' (%p) exec to '%s'\n",
	     service->priv->id, service, service->priv->exec);

  g_object_notify (G_OBJECT (service), "exec");
}

gboolean
gsm_service_get_enabled (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), FALSE);

  return service->priv->enabled;
}

static void
handle_toggle_key_changed (GConfClient *client,
			   guint        cnxn_id,
			   GConfEntry  *entry,
			   GsmService  *service)
{
  gboolean enabled;

  gsm_debug (SERVICES, "Service '%s' (%p) received notification that GConf key '%s' changed\n",
	     service->priv->id, service, entry->key);

  enabled = service->priv->enabled;

  if (service->priv->enabled_key &&
      strcmp (entry->key, service->priv->enabled_key) == 0)
    {
      if (entry->value && entry->value->type == GCONF_VALUE_BOOL)
	{
	  enabled = gconf_value_get_bool (entry->value);
	}
      else
	{
	  gsm_warning ("EnabledKey '%s' for service '%s' set to invalid value; disabling",
		       service->priv->enabled_key, service->priv->id);
	  enabled = FALSE;
	}
    }
  else if (service->priv->disabled_key &&
	   strcmp (entry->key, service->priv->disabled_key) == 0)
    {
      if (entry->value && entry->value->type == GCONF_VALUE_BOOL)
	{
	  enabled = !gconf_value_get_bool (entry->value);
	}
      else
	{
	  gsm_warning ("DisabledKey '%s' for service '%s' set to invalid value; enabling",
		       service->priv->disabled_key, service->priv->id);
	  enabled = TRUE;
	}
    }

  enabled = enabled != FALSE;

  if (service->priv->enabled != enabled)
    {
      gsm_debug (SERVICES, "Setting value of enabled flag for service '%s' (%p) to %s\n",
		 service->priv->id, service, enabled ? "(true)" : "(false)");
      service->priv->enabled = enabled;
      g_object_notify (G_OBJECT (service), "enabled");
    }
}

static char *
get_gconf_key_dirname (const char *key)
{
  char *dirname;
  char *p;

  g_assert (gconf_valid_key (key, NULL));
  g_assert (strchr (key, '/') != NULL);

  dirname = g_strdup (key);

  p = strrchr (dirname, '/');
  *p = '\0';

  if (dirname[0] == '\0')
    {
      g_free (dirname);
      dirname = g_strdup ("/");
    }

  return dirname;
}

static void
add_toggle_key_notify (GsmService *service,
		       GConfClient *client,
		       const char  *key)
{
  char *dirname;

  dirname = get_gconf_key_dirname (key);
  gconf_client_add_dir (client, dirname, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
  g_free (dirname);

  service->priv->toggle_key_notify =
    gconf_client_notify_add (client,
			     key,
			     (GConfClientNotifyFunc) handle_toggle_key_changed,
			     service,
			     NULL,
			     NULL);
}

static void
update_toggle_key (GsmService *service)
{
  GConfClient *client;
  gboolean     enabled;

  enabled = service->priv->enabled;

  client = gsm_gconf_get_client ();

  if (service->priv->toggle_key_notify != 0)
    {
      gconf_client_notify_remove (client, service->priv->toggle_key_notify);
      service->priv->toggle_key_notify = 0;
    }

  if (service->priv->enabled_key != NULL)
    {
      GError *error;

      add_toggle_key_notify (service, client, service->priv->enabled_key);

      error = NULL;
      enabled = gconf_client_get_bool (client,
				       service->priv->enabled_key,
				       &error);
      if (error != NULL)
	{
	  gsm_warning ("Error reading value of EnabledKey '%s': %s",
		       service->priv->enabled_key, error->message);
	  g_error_free (error);
	}
      else
	{
	  gsm_debug (SERVICES, "EnabledKey '%s' is set to %s\n",
		     service->priv->enabled_key, enabled ? "(true)" : "(false)");
	}
    }
  else if (service->priv->disabled_key != NULL)
    {
      GError *error;

      add_toggle_key_notify (service, client, service->priv->disabled_key);

      error = NULL;
      enabled = !gconf_client_get_bool (client,
					service->priv->disabled_key,
					&error);
      if (error != NULL)
	{
	  gsm_warning ("Error reading value of DisabledKey '%s': %s",
		       service->priv->disabled_key, error->message);
	  g_error_free (error);
	}
      else
	{
	  gsm_debug (SERVICES, "DisabledKey '%s' is set to %s\n",
		     service->priv->disabled_key, enabled ? "(false)" : "(true)");
	}
    }

  enabled = enabled != FALSE;

  if (service->priv->enabled != enabled)
    {
      gsm_debug (SERVICES, "Setting value of enabled flag for service '%s' (%p) to %s\n",
		 service->priv->id, service, enabled ? "(true)" : "(false)");
      service->priv->enabled = enabled;
      g_object_notify (G_OBJECT (service), "enabled");
    }
}

const char *
gsm_service_get_enabled_key (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->enabled_key;
}

void
gsm_service_set_enabled_key (GsmService *service,
			     const char *enabled_key)
{
  g_return_if_fail (GSM_IS_SERVICE (service));

  if ((service->priv->enabled_key == NULL && enabled_key == NULL) ||
      (service->priv->enabled_key != NULL && enabled_key != NULL &&
       strcmp (service->priv->enabled_key, enabled_key) == 0))
    return;

  g_free (service->priv->enabled_key);
  service->priv->enabled_key = g_strdup (enabled_key);

  gsm_debug (SERVICES, "Set service '%s' (%p) enabled-key to '%s'\n",
	     service->priv->id, service,
	     service->priv->enabled_key ? service->priv->enabled_key : "(null)");

  update_toggle_key (service);

  g_object_notify (G_OBJECT (service), "enabled-key");
}

const char *
gsm_service_get_disabled_key (GsmService *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), NULL);

  return service->priv->disabled_key;
}

void
gsm_service_set_disabled_key (GsmService *service,
			      const char *disabled_key)
{
  g_return_if_fail (GSM_IS_SERVICE (service));

  if ((service->priv->disabled_key == NULL && disabled_key == NULL) ||
      (service->priv->disabled_key != NULL && disabled_key != NULL &&
       strcmp (service->priv->disabled_key, disabled_key) == 0))
    return;

  g_free (service->priv->disabled_key);
  service->priv->disabled_key = g_strdup (disabled_key);

  gsm_debug (SERVICES, "Set service '%s' (%p) disabled-key to '%s'\n",
	     service->priv->id, service,
	     service->priv->disabled_key ? service->priv->disabled_key : "(null)");

  update_toggle_key (service);

  g_object_notify (G_OBJECT (service), "disabled-key");
}

static void
emit_startup_completed (GsmService *service)
{
  g_signal_emit (service, signals[STARTUP_COMPLETED], 0);
}

static void
gsm_service_handle_startup_completed (GsmService *service)
{
  if (service->priv->startup_monitor != NULL)
    {
      g_signal_handler_disconnect (service->priv->startup_monitor,
				   service->priv->startup_completed_handler);
      service->priv->startup_completed_handler = 0;

      g_object_unref (service->priv->startup_monitor);
      service->priv->startup_monitor = NULL;
    }

  if (service->priv->sn_context != NULL)
    sn_launcher_context_unref (service->priv->sn_context);
  service->priv->sn_context = NULL;

  if (service->priv->sn_timeout != 0)
    g_source_remove (service->priv->sn_timeout);
  service->priv->sn_timeout = 0;

  emit_startup_completed (service);
}

static gboolean
gsm_service_sn_timeout (GsmService *service)
{
  GTimeVal now;
  long     tv_sec, tv_usec;
  double   elapsed;

  gsm_debug (SERVICES, "Timed out waiting for startup-complete notification for '%s' (%p)\n",
	     service->priv->id, service);

  g_get_current_time (&now);

  sn_launcher_context_get_last_active_time (service->priv->sn_context, &tv_sec, &tv_usec);

  elapsed = ((double)((now.tv_sec - tv_sec) * G_USEC_PER_SEC + (now.tv_usec - tv_usec))) / 1000.0;

  if (elapsed < GSM_SERVICE_SN_TIMEOUT)
    {
      gsm_debug (SERVICES, "Only %f seconds elapsed; waiting %f more for '%s' (%p)\n",
		 elapsed, GSM_SERVICE_SN_TIMEOUT - elapsed, service->priv->id, service);
      service->priv->sn_timeout = g_timeout_add (GSM_SERVICE_SN_TIMEOUT - elapsed,
						 (GSourceFunc) gsm_service_sn_timeout,
						 service);
      return FALSE;
    }
  else
    {
      service->priv->sn_timeout = 0;

      /* FIXME:
       *  Really, we should go to startup-complete so that all the
       *  other services can be launched but after another, longer
       *  timeout have an "the app isn't responding, ignore it,
       *  wait a bit, try and restart it, logout" (or something)
       *  dialog to handle the case where the app has hung
       */
      sn_launcher_context_complete (service->priv->sn_context);

      gsm_service_handle_startup_completed (service);
    }

  return FALSE;
}

static void gsm_service_respawn (GsmService *service);

static void
gsm_service_handle_too_many_respawns_response (GsmService *service,
					       guint       response_id,
					       GtkWidget  *dialog)
{
  gtk_widget_destroy (dialog);

  switch (response_id)
    {
    case GSM_RESPONSE_LOGOUT:
      /* FIXME: implement */
      break;

    case GSM_RESPONSE_KEEP_TRYING:
      service->priv->last_respawn_time = 0;
      service->priv->respawn_count = 0;
      gsm_service_respawn (service);
      break;

    case GSM_RESPONSE_DISABLE:
    default:
      break;
    }
}

static void
gsm_service_respawn (GsmService *service)
{
  GError *error;

  gsm_debug (SERVICES, "Attempting to re-spawn service '%s' (%p)\n",
	     service->priv->id, service);

  if (service->priv->respawn_count > GSM_SERVICE_MAX_RESPAWNS)
    {
      GTime now;

      now = time (NULL);
      if (now > (service->priv->last_respawn_time + GSM_SERVICE_MAX_RESPAWNS_TIMEOUT))
	{
	  service->priv->last_respawn_time = 0;
	  service->priv->respawn_count = 0;
	}
    }

  if (service->priv->respawn_count > GSM_SERVICE_MAX_RESPAWNS)
    {
      gsm_warning ("Re-spawned service '%s' too many times, giving up",
		   service->priv->id);

      if (service->priv->error_dialog != NULL)
	gtk_widget_destroy (service->priv->error_dialog);
      service->priv->error_dialog = NULL;

      service->priv->error_dialog = gsm_show_too_many_respawns_dialog (service->priv->name);

      g_signal_connect_swapped (service->priv->error_dialog, "response",
				G_CALLBACK (gsm_service_handle_too_many_respawns_response), service);

      g_object_add_weak_pointer (G_OBJECT (service->priv->error_dialog),
				 (gpointer *) &service->priv->error_dialog);

      return;
    }

  service->priv->respawn_count++;
  service->priv->last_respawn_time = time (NULL);

  error = NULL;
  if (!gsm_service_start (service, &error))
    {
      gsm_warning ("Failed to start service '%s': %s",
		   service->priv->id, error->message);

      gsm_service_show_error_dialog (service, error);

      g_error_free (error);
    }
}

static void
gsm_service_child_watch_handler (GPid        pid,
				 int         status,
				 GsmService *service)
{
  gsm_debug (SERVICES, "Received SIGCHLD from pid %d; service '%s' (%p) died\n",
	     pid, service->priv->id, service);

  service->priv->pid = 0;
  service->priv->child_watch = 0;

  gsm_service_handle_startup_completed (service);

  gsm_service_respawn (service);
}

static char **
gsm_service_make_environment (SnLauncherContext *sn_context)
{
  char     **envp;
  int        i;
  gboolean   got_desktop_startup_id = FALSE;

  envp = g_listenv ();

  for (i = 0; envp[i] != NULL; i++)
    {
      char       *name = envp[i];
      const char *value;

      if (!strcmp (name, "DESKTOP_STARTUP_ID"))
	{
	  value = sn_launcher_context_get_startup_id (sn_context);
	  got_desktop_startup_id = TRUE;
	}
      else
	{
	  value = g_getenv (name);
	}

      envp[i] = g_strjoin ("=", name, value, NULL);

      g_free (name);
    }

  if (!got_desktop_startup_id)
    {
      envp = g_renew (char *, envp, i + 2);

      envp[i++] = g_strdup_printf ("DESKTOP_STARTUP_ID=%s",
				   sn_launcher_context_get_startup_id (sn_context));
      envp[i++] = NULL;
    }

  return envp;
}

static SnLauncherContext *
gsm_service_create_sn_context (GsmService *service,
			       const char *name,
			       const char *icon_name,
			       const char *binary_name)
{
  SnLauncherContext *sn_context;
  SnDisplay         *sn_display;
  Display           *xdisplay;
  char              *description;

  sn_display = gsm_startup_monitor_get_sn_display (service->priv->startup_monitor);

  xdisplay = sn_display_get_x_display (sn_display);

  sn_context = sn_launcher_context_new (sn_display, DefaultScreen (xdisplay));

  sn_launcher_context_set_silent (sn_context, TRUE);

  sn_launcher_context_set_name (sn_context, name);

  description = g_strdup_printf (_("Starting %s"), name);
  sn_launcher_context_set_description (sn_context, description);
  g_free (description);

  if (icon_name != NULL)
    sn_launcher_context_set_icon_name (sn_context, icon_name);

  sn_launcher_context_set_binary_name (sn_context, binary_name);

  return sn_context;
}

gboolean
gsm_service_start (GsmService  *service,
		   GError     **err)
{
  GError    *error;
  gboolean   retval;
  char     **argv;
  char     **envp;
  int        argc;
  char      *signal_name;

  g_return_val_if_fail (GSM_IS_SERVICE (service), FALSE);
  g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

  g_assert (service->priv->id != NULL);
  g_assert (service->priv->name != NULL);
  g_assert (service->priv->exec != NULL);

  gsm_debug (SERVICES, "Starting service '%s' (%p): exec = '%s'\n",
	     service->priv->id, service, service->priv->exec);

  if (service->priv->pid != 0)
    {
      gsm_debug (SERVICES, "Service '%s' (%p) already running with PID %d\n",
		 service->priv->id, service, service->priv->pid);
      return TRUE;
    }

  argc = 0;
  argv = NULL;
  error = NULL;
  if (!g_shell_parse_argv (service->priv->exec, &argc, &argv, &error))
    {
      gsm_warning ("Failed to parse command line '%s' for service '%s': %s",
		   service->priv->exec, service->priv->id, error->message);
      g_propagate_error (err, error);
      return FALSE;
    }

  service->priv->startup_monitor = gsm_startup_monitor_get ();

  service->priv->sn_context = gsm_service_create_sn_context (service,
							     service->priv->name,
							     service->priv->icon_name,
							     argv[0]);

  /* We can't do any better than CurrentTime here - this wasn't
   * initiated by any X event.
   */
  sn_launcher_context_initiate (service->priv->sn_context,
				g_get_prgname (),
				argv[0],
				CurrentTime);

  envp = gsm_service_make_environment (service->priv->sn_context);

  /* DO_NOT_REAP_CHILD is needed for the child watch to work
   */
  retval = TRUE;
  error = NULL;
  if (!g_spawn_async (g_get_home_dir (),
		      argv,
		      envp,
		      G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
		      (GSpawnChildSetupFunc) NULL,
		      NULL,
		      &service->priv->pid,
		      &error))
    {
      gsm_warning ("Failed to launch service '%s' with command line '%s': %s",
		   service->priv->id, service->priv->exec, error->message);
      g_propagate_error (err, error);

      sn_launcher_context_complete (service->priv->sn_context);
      sn_launcher_context_unref (service->priv->sn_context);
      service->priv->sn_context = NULL;

      g_object_unref (service->priv->startup_monitor);
      service->priv->startup_monitor = NULL;

      retval = FALSE;
      goto out;
    }

  gsm_debug (SERVICES, "Successfully launched service '%s' (%p): pid = %d\n",
	     service->priv->id, service, service->priv->pid);

  service->priv->child_watch = g_child_watch_add (service->priv->pid,
						  (GChildWatchFunc) gsm_service_child_watch_handler,
						  service);

  service->priv->sn_timeout = g_timeout_add (GSM_SERVICE_SN_TIMEOUT,
					     (GSourceFunc) gsm_service_sn_timeout,
					     service);

  signal_name = g_strjoin ("::",
			   "startup-completed",
			   sn_launcher_context_get_startup_id (service->priv->sn_context),
			   NULL);

  service->priv->startup_completed_handler =
    g_signal_connect_swapped (service->priv->startup_monitor, signal_name,
			      G_CALLBACK (gsm_service_handle_startup_completed), service);

  g_free (signal_name);
						
 out:
  g_strfreev (envp);
  g_strfreev (argv);

  return retval;
}

gboolean
gsm_service_stop (GsmService  *service)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), FALSE);

  gsm_debug (SERVICES, "Stopping service '%s' (%p): pid = %d\n",
	     service->priv->id, service, service->priv->pid);

  if (service->priv->error_dialog != NULL)
    gtk_widget_destroy (service->priv->error_dialog);
  service->priv->error_dialog = NULL;

  if (service->priv->startup_completed_handler != 0)
    {
      g_assert (service->priv->startup_monitor != NULL);
      g_signal_handler_disconnect (service->priv->startup_monitor,
				   service->priv->startup_completed_handler);
      service->priv->startup_completed_handler = 0;
    }

  if (service->priv->sn_timeout != 0)
    g_source_remove (service->priv->sn_timeout);
  service->priv->sn_timeout = 0;

  if (service->priv->child_watch != 0)
    g_source_remove (service->priv->child_watch);
  service->priv->child_watch = 0;

  if (service->priv->pid != 0)
    kill (service->priv->pid, SIGKILL);
  service->priv->pid = 0;

  if (service->priv->sn_context != NULL)
    {
      sn_launcher_context_complete (service->priv->sn_context);
      sn_launcher_context_unref (service->priv->sn_context);
      service->priv->sn_context = NULL;
    }

  if (service->priv->startup_monitor != NULL)
    g_object_unref (service->priv->startup_monitor);
  service->priv->startup_monitor = NULL;

  return TRUE;
}

gboolean
gsm_service_restart (GsmService  *service,
		     GError     **err)
{
  g_return_val_if_fail (GSM_IS_SERVICE (service), FALSE);
  g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

  if (!gsm_service_stop (service))
    return FALSE;

  return gsm_service_start (service, err);
}

void
gsm_service_show_error_dialog (GsmService *service,
			       GError     *error)
{
  GsmFailedService *loser;

  g_return_if_fail (GSM_IS_SERVICE (service));
  g_return_if_fail (error != NULL);

  if (service->priv->error_dialog != NULL)
    return;

  loser = gsm_failed_service_new (service->priv->name, error->message);

  service->priv->error_dialog = gsm_show_failed_service_dialog (loser);

  g_object_add_weak_pointer (G_OBJECT (service->priv->error_dialog),
			     (gpointer *) &service->priv->error_dialog);

  gsm_failed_service_free (loser);
}
