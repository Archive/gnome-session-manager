/*
 * Copyright (C) 2005 Red Hat, Inc.
 * Copyright (C) 2001 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 *      Havoc Pennington <hp@redhat.com>
 */

#include <config.h>

#include "gsm-config.h"

#include <string.h>
#include <glib/gi18n.h>

#include "gsm-debug.h"

typedef enum
{
  GSM_CONFIG_INIT = 0,
  GSM_CONFIG_IN_CONFIG,
  GSM_CONFIG_IN_SERVICES_DIR,
  GSM_CONFIG_IN_CORE_SERVICES,
  GSM_CONFIG_IN_SERVICE,
  GSM_CONFIG_END
} GsmConfigState;

struct _GsmConfig
{
  GsmConfigState  state;
  GSList         *services_dirs;
  GSList         *core_services;
};

static void
set_error (GError              **error,
	   GMarkupParseContext  *context,
	   const char           *format,
	   ...)
{
  va_list  args;
  char    *str;
  int      l, c;

  g_return_if_fail (format != NULL);

  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);

  g_markup_parse_context_get_position (context, &l, &c);

  g_set_error (error,
	       GSM_CONFIG_ERROR,
	       GSM_CONFIG_ERROR_FAILED,
               _("line %d character %d: %s"),
               l, c, str);

  g_free (str);
}

static gboolean
check_no_attributes (GError              **error,
		     GMarkupParseContext  *context,
		     const char          **attribute_names,
		     const char           *element_name)
{
  if (attribute_names[0] != NULL)
    {
      set_error (error,
		 context,
		 _("Attributes are not allowed on the <%s> element"),
		 element_name);
      return FALSE;
    }
  else
    {
      return TRUE;
    }
}

static void
unexpected_close_tag (GError              **error,
		      GMarkupParseContext  *context,
		      const char           *element_name)
{
  set_error (error,
	     context,
	     _("Unexpected close element tag </%s>"),
	     element_name);
}

static void
check_only_whitespace (GError **error,
		       GMarkupParseContext *context,
		       const char          *text,
		       int                  text_len)
{
  const char *p;

  for (p = text; p < text + text_len; p++)
    {
      if (!g_ascii_isspace (*p))
	{
	  set_error (error, context, _("Unexpected text"));
	  break;
	}
    }
}

static void
gsm_config_start_element (GMarkupParseContext  *context,
			  const gchar          *element_name,
			  const gchar         **attribute_names,
			  const gchar         **attribute_values,
			  gpointer              user_data,
			  GError              **error)
{
  GsmConfig *config = user_data;

  switch (config->state)
    {
    case GSM_CONFIG_INIT:
      if (strcmp (element_name, "gsm_config") == 0)
	{
	  if (check_no_attributes (error, context, attribute_names, element_name))
	    {
	      config->state = GSM_CONFIG_IN_CONFIG;
	    }
	}
      else
	{
	  set_error (error,
		     context,
		     _("File does not appear to be a GNOME session manager config file; outermost element is <%s> rather than <gsm_config>"),
		     element_name);
	}
      break;

    case GSM_CONFIG_IN_CONFIG:
      if (strcmp (element_name, "services_dir") == 0)
	{
	  if (check_no_attributes (error, context, attribute_names, element_name))
	    {
	      config->state = GSM_CONFIG_IN_SERVICES_DIR;
	    }
	}
      else if (strcmp (element_name, "core_services") == 0)
	{
	  if (check_no_attributes (error, context, attribute_names, element_name))
	    {
	      config->state = GSM_CONFIG_IN_CORE_SERVICES;
	    }
	}
      else
	{
	  set_error (error,
		     context,
		     _("Element <%s> may not appear inside a <gsm_config> element"),
		     element_name);
	}
      break;

    case GSM_CONFIG_IN_CORE_SERVICES:
      if (strcmp (element_name, "service") == 0)
	{
	  if (check_no_attributes (error, context, attribute_names, element_name))
	    {
	      config->state = GSM_CONFIG_IN_SERVICE;
	    }
	}
      else
	{
	  set_error (error,
		     context,
		     _("Element <%s> may not appear inside a <core_services> element"),
		     element_name);
	}
      break;

    case GSM_CONFIG_IN_SERVICES_DIR:
    case GSM_CONFIG_IN_SERVICE:
      set_error (error,
		 context,
		 _("No elements allowed inside the <%s> element"),
		 element_name);
      break;

    case GSM_CONFIG_END:
      set_error (error,
		 context,
		 _("Element <%s> appears after the <gsm_config> element"),
		 element_name);
      break;
    }
}

static void
gsm_config_end_element (GMarkupParseContext  *context,
			const gchar          *element_name,
			gpointer              user_data,
			GError              **error)
{
  GsmConfig *config = user_data;

  switch (config->state)
    {
    case GSM_CONFIG_INIT:
    case GSM_CONFIG_END:
      unexpected_close_tag (error, context, element_name);
      break;

    case GSM_CONFIG_IN_CONFIG:
      if (strcmp (element_name, "gsm_config") == 0)
	{
	  config->state = GSM_CONFIG_END;
	}
      else
	{
	  unexpected_close_tag (error, context, element_name);
	}
      break;

    case GSM_CONFIG_IN_SERVICES_DIR:
      if (strcmp (element_name, "services_dir") == 0)
	{
	  config->state = GSM_CONFIG_IN_CONFIG;
	}
      else
	{
	  unexpected_close_tag (error, context, element_name);
	}
      break;

    case GSM_CONFIG_IN_CORE_SERVICES:
      if (strcmp (element_name, "core_services") == 0)
	{
	  config->state = GSM_CONFIG_IN_CONFIG;
	}
      else
	{
	  unexpected_close_tag (error, context, element_name);
	}
      break;

    case GSM_CONFIG_IN_SERVICE:
      if (strcmp (element_name, "service") == 0)
	{
	  config->state = GSM_CONFIG_IN_CORE_SERVICES;
	}
      else
	{
	  unexpected_close_tag (error, context, element_name);
	}
      break;
    }
}

static void
gsm_config_text (GMarkupParseContext  *context,
		 const gchar          *text,
		 gsize                 text_len,  
		 gpointer              user_data,
		 GError              **error)
{
  GsmConfig *config = user_data;

  switch (config->state)
    {
    case GSM_CONFIG_IN_SERVICES_DIR:
      {
	char *services_dir;

	services_dir = g_strndup (text, text_len);

	gsm_debug (CONFIG, "Got services dir '%s'\n", services_dir);

	config->services_dirs = g_slist_append (config->services_dirs,
						services_dir);
      }
      break;

    case GSM_CONFIG_IN_SERVICE:
      {
	char *service;

	service = g_strndup (text, text_len);

	gsm_debug (CONFIG, "Got core service '%s'\n", service);

	config->core_services = g_slist_append (config->core_services,
						service);
      }
      break;

    case GSM_CONFIG_INIT:
    case GSM_CONFIG_IN_CONFIG:
    case GSM_CONFIG_IN_CORE_SERVICES:
    case GSM_CONFIG_END:
      check_only_whitespace (error, context, text, text_len);
      break;
    }
}

static const GMarkupParser gsm_config_parser =
{
  gsm_config_start_element,
  gsm_config_end_element,
  gsm_config_text,
  NULL,
  NULL
};

GsmConfig *
gsm_config_parse (const char  *path,
		  GError     **err)
{
  GMarkupParseContext *context;
  GsmConfig           *config;
  GError              *error;
  char                *contents;
  gsize                length;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (err == NULL || *err == NULL, NULL);

  gsm_debug (CONFIG, "Parsing config file '%s'\n", path);

  contents = NULL;
  length = 0;
  error = NULL;
  if (!g_file_get_contents (path, &contents, &length, &error))
    {
      gsm_warning ("Failed to read '%s': %s", path, error->message);
      g_propagate_error (err, error);
      return NULL;
    }

  config = g_new0 (GsmConfig, 1);

  config->state = GSM_CONFIG_INIT;

  context = g_markup_parse_context_new (&gsm_config_parser, 0, config, NULL);

  error = NULL;
  if (!g_markup_parse_context_parse (context, contents, length, &error))
    goto parse_error;

  error = NULL;
  if (!g_markup_parse_context_end_parse (context, &error))
    goto parse_error;

  g_markup_parse_context_free (context);

  g_free (contents);

  return config;

 parse_error:
  gsm_warning ("Failed to parse '%s': %s", path, error->message);
  g_propagate_error (err, error);
  g_free (config);
  g_free (contents);
  return NULL;
}

void
gsm_config_free (GsmConfig *config)
{
  g_return_if_fail (config != NULL);

  g_slist_foreach (config->core_services,
		   (GFunc) g_free,
		   NULL);
  g_slist_free (config->core_services);

  g_slist_foreach (config->services_dirs,
		   (GFunc) g_free,
		   NULL);
  g_slist_free (config->services_dirs);

  g_free (config);
}

GSList *
gsm_config_get_services_dirs (GsmConfig *config)
{
  g_return_val_if_fail (config != NULL, NULL);

  return config->services_dirs;
}

GSList *
gsm_config_get_core_services (GsmConfig *config)
{
  g_return_val_if_fail (config != NULL, NULL);

  return config->core_services;
}
