/*
 * Copyright (C) 2005 Red Hat, Inc.
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>

#include "gsm-debug.h"

#ifdef G_ENABLE_DEBUG
GsmDebugFlags _gsm_debug_flags = GSM_DEBUG_NONE;

void
gsm_setup_debug_flags (void)
{
  const char       *env_str;
  static GDebugKey  debug_keys [] =
    {
      { "session",         GSM_DEBUG_SESSION         },
      { "config",          GSM_DEBUG_CONFIG          },
      { "services",        GSM_DEBUG_SERVICES        },
      { "startup-monitor", GSM_DEBUG_STARTUP_MONITOR },
      { "gconf",           GSM_DEBUG_GCONF           },
      { "logout",          GSM_DEBUG_LOGOUT          }
    };
  
  env_str = g_getenv ("GSM_DEBUG");
  
  if (env_str)
    _gsm_debug_flags |= g_parse_debug_string (env_str,
					      debug_keys,
					      G_N_ELEMENTS (debug_keys));
}
#endif /* G_ENABLE_DEBUG */

void
gsm_warning (const char *format, ...)
{
  va_list args;

  va_start (args, format);
  g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, format, args);
  va_end (args);
}
