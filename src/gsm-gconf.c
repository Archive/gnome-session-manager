/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>

#include "gsm-gconf.h"

#include <sys/types.h>
#include <sys/wait.h>

#include "gsm-debug.h"

static GConfClient *global_gconf_client = NULL;

GConfClient *
gsm_gconf_get_client (void)
{
  if (global_gconf_client == NULL)
    {
      gsm_debug (GCONF, "Initializing global GConf client\n");
      global_gconf_client = gconf_client_get_default ();
    }

  return global_gconf_client;
}

void
gsm_gconf_shutdown (void)
{
  GError *error;
  char   *command;
  int     status;

  if (global_gconf_client != NULL)
    {
      gsm_debug (GCONF, "Unreffing global GConf client\n");
      g_object_unref (global_gconf_client);
      global_gconf_client = NULL;
    }

  command = g_strjoin (" ", GCONFTOOL, "--shutdown", NULL);

  gsm_debug (GCONF, "Running '%s' to shutdown gconfd\n", command);

  status = 0;
  error  = NULL;
  if (!g_spawn_command_line_sync (command, NULL, NULL, &status, &error))
    {
      gsm_warning ("Failed to execute '%s' on logout: %s",
		   command, error->message);
      g_error_free (error);
    }

  if (status)
    {
      gsm_warning ("Running '%s' at logout returned an exit status of '%d'",
		   command, status);
    }

  g_free (command);
}

gboolean
gsm_gconf_run_sanity_check (void)
{
  GError *error;
  int     status;

  gsm_debug (GCONF, "Running " GCONF_SANITY_CHECK "\n");

  error = NULL;
  if (!g_spawn_command_line_sync (GCONF_SANITY_CHECK, NULL, NULL, &status, &error))
    {
      gsm_warning ("Failed to run gconf-sanity-check-2: %s\n",
		   error->message);
      g_error_free (error);
      return TRUE; /* just continue, who knows what went wrong */
    }

  if (WIFEXITED (status) && WEXITSTATUS (status) != 0)
    {
      gsm_warning ("gconf-sanity-check-2 did not pass, logging back out");
      return FALSE;
    }

  gsm_debug (GCONF, GCONF_SANITY_CHECK " completed successfully\n");

  return TRUE;
}
