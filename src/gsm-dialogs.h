/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GSM_DIALOGS_H__
#define __GSM_DIALOGS_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef enum
{
  GSM_RESPONSE_LOGOUT,
  GSM_RESPONSE_KEEP_TRYING,
  GSM_RESPONSE_DISABLE
} GsmResponseType;

typedef struct _GsmFailedService GsmFailedService;

void gsm_run_login_failed_dialog (const char *primary_text,
				  const char *secondary_text);

GtkWidget *gsm_show_too_many_respawns_dialog (const char *service_name);

GsmFailedService *gsm_failed_service_new  (const char       *service_name,
					   const char       *error_text);
void              gsm_failed_service_free (GsmFailedService *loser);

GtkWidget *gsm_show_failed_service_dialog  (GsmFailedService *loser);
GtkWidget *gsm_show_failed_services_dialog (GSList           *failed_services);

G_END_DECLS

#endif /* __GSM_DIALOGS_H__ */
