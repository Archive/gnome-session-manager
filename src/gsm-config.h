/*
 * Copyright (C) 2005 Red Hat, Inc.
 * Copyright (C) 2001 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 *      Havoc Pennington <hp@redhat.com>
 */

#ifndef __GSM_CONFIG_H__
#define __GSM_CONFIG_H__

#include <glib.h>

G_BEGIN_DECLS

#define GSM_CONFIG_ERROR (g_quark_from_static_string ("gsm-config-error-quark"))

typedef enum
{
  GSM_CONFIG_ERROR_FAILED
} GsmConfigError;

typedef struct _GsmConfig GsmConfig;

GsmConfig *gsm_config_parse (const char  *path,
			     GError     **error);
void       gsm_config_free  (GsmConfig   *config);

GSList *gsm_config_get_services_dirs (GsmConfig *config);
GSList *gsm_config_get_core_services (GsmConfig *config);

G_END_DECLS

#endif /* __GSM_CONFIG_H__ */
