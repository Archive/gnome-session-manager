/*
 * Copyright (C) 2005 Red Hat, Inc.
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GSM_DEBUG_H__
#define __GSM_DEBUG_H__

#include <glib.h>

G_BEGIN_DECLS

void gsm_warning (const char *format, ...) G_GNUC_PRINTF (1, 2);

#ifdef G_ENABLE_DEBUG

typedef enum
{
  GSM_DEBUG_NONE            = 0,
  GSM_DEBUG_SESSION         = 1 << 0,
  GSM_DEBUG_CONFIG          = 1 << 1,
  GSM_DEBUG_SERVICES        = 1 << 2,
  GSM_DEBUG_STARTUP_MONITOR = 1 << 3,
  GSM_DEBUG_GCONF           = 1 << 4,
  GSM_DEBUG_LOGOUT          = 1 << 5
} GsmDebugFlags;

extern GsmDebugFlags _gsm_debug_flags;

#ifdef G_HAVE_ISO_VARARGS
# define gsm_debug(type, ...) G_STMT_START       \
    {                                            \
      if (_gsm_debug_flags & GSM_DEBUG_##type)   \
	{                                        \
	  g_printerr (__VA_ARGS__);              \
	}                                        \
    } G_STMT_END
#elif defined(G_HAVE_GNUC_VARARGS)
# define gsm_debug(type, args...) G_STMT_START   \
    {                                            \
      if (_gsm_debug_flags & GSM_DEBUG_##type)   \
	{                                        \
	  g_printerr (args);                     \
	}                                        \
    } G_STMT_END
#endif

void gsm_setup_debug_flags (void);

#else /* if !defined (G_ENABLE_DEBUG) */

#ifdef G_HAVE_ISO_VARARGS
#  define gsm_debug(...)
#elif defined(G_HAVE_GNUC_VARARGS)
#  define gsm_debug(args...)
#endif

#define gsm_setup_debug_flags()

#endif /* G_ENABLE_DEBUG */

G_END_DECLS

#endif /* __GSM_DEBUG_H__ */
