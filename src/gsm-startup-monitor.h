/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GSM_STARTUP_MONITOR_H__
#define __GSM_STARTUP_MONITOR_H__

#include <glib-object.h>
#include <libsn/sn.h>

G_BEGIN_DECLS

#define GSM_TYPE_STARTUP_MONITOR         (gsm_startup_monitor_get_type ())
#define GSM_STARTUP_MONITOR(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GSM_TYPE_STARTUP_MONITOR, GsmStartupMonitor))
#define GSM_STARTUP_MONITOR_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GSM_TYPE_STARTUP_MONITOR, GsmStartupMonitorClass))
#define GSM_IS_STARTUP_MONITOR(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GSM_TYPE_STARTUP_MONITOR))
#define GSM_IS_STARTUP_MONITOR_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GSM_TYPE_STARTUP_MONITOR))
#define GSM_STARTUP_MONITOR_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GSM_TYPE_STARTUP_MONITOR, GsmStartupMonitorClass))

typedef struct _GsmStartupMonitor        GsmStartupMonitor;
typedef struct _GsmStartupMonitorClass   GsmStartupMonitorClass;
typedef struct _GsmStartupMonitorPrivate GsmStartupMonitorPrivate;

struct _GsmStartupMonitor
{
  GObject object;

  GsmStartupMonitorPrivate *priv;
};

struct _GsmStartupMonitorClass
{
  GObjectClass object_class;

  void (* startup_completed) (GsmStartupMonitor *monitor);
};

GType gsm_startup_monitor_get_type (void) G_GNUC_CONST;

GsmStartupMonitor *gsm_startup_monitor_get (void);

SnDisplay *gsm_startup_monitor_get_sn_display (GsmStartupMonitor *monitor);

G_END_DECLS

#endif /* __GSM_STARTUP_MONITOR_H__ */
