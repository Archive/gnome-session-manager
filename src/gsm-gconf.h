/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GSM_GCONF_H__
#define __GSM_GCONF_H__

#include <gconf/gconf-client.h>

G_BEGIN_DECLS

GConfClient *gsm_gconf_get_client       (void);
void         gsm_gconf_shutdown         (void);
gboolean     gsm_gconf_run_sanity_check (void);

G_END_DECLS

#endif /* __GSM_GCONF_H__ */
