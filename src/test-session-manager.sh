#!/bin/bash

xauth list | grep '/unix:0' | while read display proto cookie; do xauth add ${display%0}1 $proto $cookie; done

Xnest -terminate :1 &
xnest_pid=$!

sleep 1

export DISPLAY=:1

output=$(dbus-launch --exit-with-session)

eval $output
echo $output

GSM_DEBUG=all ./gnome-session-manager

sleep 1

kill $xnest_pid
