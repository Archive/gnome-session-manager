/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>

#include "gsm-session.h"

#include <glib/gi18n.h>
#include <string.h>
#include <gtk/gtk.h>

#include "gsm-config.h"
#include "gsm-dialogs.h"
#include "gsm-logout.h"
#include "gsm-service.h"
#include "gsm-debug.h"

/* FIXME: I don't think we should need to close before
 * unrefing for the last time, but if we do, there should
 * at least be an API in the glib bindings:
 *  http://lists.freedesktop.org/archives/dbus/2005-September/thread.html#3327
 */
#ifndef FIXME
#define DBUS_API_SUBJECT_TO_CHANGE 1
#include <dbus/dbus-glib-lowlevel.h>
#define dbus_g_connection_close(cnx) dbus_connection_close (dbus_g_connection_get_connection (cnx))
#endif

struct _GsmSessionPrivate
{
  GsmConfig  *config;
  GHashTable *services;

  DBusGConnection *bus;

  GSList *pending_core_services;
  GSList *pending_other_services;
  GSList *failed_services;

  GtkWidget *failed_services_dialog;

  guint is_running : 1;
};

#define GSM_SESSION_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), GSM_TYPE_SESSION, GsmSessionPrivate))

static gboolean gsm_session_dbus_logout          (GsmSession   *session,
						  GError      **error);
static gboolean gsm_session_dbus_quit            (GsmSession   *session,
						  GError      **error);
static gboolean gsm_session_dbus_list_services   (GsmSession   *session,
						  char       ***service_ids_ret,
						  GError      **error);
static gboolean gsm_session_dbus_start_service   (GsmSession   *session,
						  const char   *service_id,
						  GError      **error);
static gboolean gsm_session_dbus_stop_service    (GsmSession   *session,
						  const char   *service_id,
						  GError      **error);
static gboolean gsm_session_dbus_restart_service (GsmSession   *session,
						  const char   *service_id,
						  GError      **error);

#include "gsm-session-dbus-glue.h"

G_DEFINE_TYPE (GsmSession, gsm_session, G_TYPE_OBJECT);

static void
gsm_session_finalize (GObject *object)
{
  GsmSession *session = GSM_SESSION (object);

  gsm_debug (SESSION, "Finalizing GsmSession %p\n", session);

  gsm_session_stop (session);

  if (session->priv->bus != NULL)
    {
      dbus_g_connection_close (session->priv->bus);
      dbus_g_connection_unref (session->priv->bus);
      session->priv->bus = NULL;
    }

  session->priv = NULL;

  G_OBJECT_CLASS (gsm_session_parent_class)->finalize (object);
}

static void
gsm_session_class_init (GsmSessionClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  gobject_class->finalize = gsm_session_finalize;

  g_type_class_add_private (gobject_class, sizeof (GsmSessionPrivate));

  dbus_g_object_type_install_info (GSM_TYPE_SESSION,
				   &dbus_glib_gsm_session_dbus_object_info);
}

static void
gsm_session_init (GsmSession *serv)
{
  serv->priv = GSM_SESSION_GET_PRIVATE (serv);
}

static void
gsm_session_nullify_singleton (GsmSession **singleton)
{
  g_return_if_fail (singleton != NULL);

  *singleton = NULL;
}

GsmSession *
gsm_session_get (void)
{
  static GsmSession *singleton = NULL;

  DBusGConnection *bus;
  DBusGProxy      *bus_proxy;
  GError          *error;
  guint            request_name_result;

  if (singleton != NULL)
    return g_object_ref (singleton);

  gsm_debug (SESSION, "Connecting to session bus\n");

  error = NULL;
  bus = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (!bus)
    {
      gsm_warning ("Couldn't connect to session bus: %s", error->message);
      gsm_run_login_failed_dialog (_("Failed to connect to the D-BUS session bus."),
				   error->message);
      g_error_free (error);
      return NULL;
    }

  bus_proxy = dbus_g_proxy_new_for_name (bus,
                                         "org.freedesktop.DBus",
                                         "/org/freedesktop/DBus",
                                         "org.freedesktop.DBus");

  error = NULL;
  if (!dbus_g_proxy_call (bus_proxy, "RequestName", &error,
                          G_TYPE_STRING, "org.gnome.SessionManager",
                          G_TYPE_UINT, DBUS_NAME_FLAG_PROHIBIT_REPLACEMENT,
                          G_TYPE_INVALID,
                          G_TYPE_UINT, &request_name_result,
                          G_TYPE_INVALID))
    {
      char *secondary_text;

      gsm_warning ("Failed to acquire org.gnome.SessionManager: %s", error->message);

      secondary_text = g_strdup_printf (_("Failed to acquire '%s' name: %s"),
					"org.gnome.SessionManager",
					error->message);
      gsm_run_login_failed_dialog (_("Failed to register with the D-BUS session bus."),
				   secondary_text);
      g_free (secondary_text);

      g_error_free (error);

      g_object_unref (bus_proxy);

      dbus_g_connection_close (bus);
      dbus_g_connection_unref (bus);
      
      return NULL;
    }

  g_object_unref (bus_proxy);

  gsm_debug (SESSION, "Creating GsmSession singleton\n");

  singleton = g_object_new (GSM_TYPE_SESSION, NULL);
  g_object_weak_ref (G_OBJECT (singleton),
		     (GWeakNotify) gsm_session_nullify_singleton,
		     &singleton);

  singleton->priv->bus = bus;

  dbus_g_connection_register_g_object (singleton->priv->bus,
				       "/org/gnome/SessionManager",
				       G_OBJECT (singleton));

  return singleton;
}

static gboolean
check_is_running (GsmSession  *session,
		  GError     **error)
{
  if (!session->priv->is_running)
    {
      g_set_error (error,
		   GSM_SESSION_ERROR,
		   GSM_SESSION_ERROR_NOT_RUNNING,
		   _("The session manager is not running"));
      return FALSE;
    }

  return TRUE;
}

static gboolean
gsm_session_dbus_logout (GsmSession  *session,
			 GError     **error)
{
  gsm_debug (SESSION, "Received Logout D-BUS message\n");

  if (!check_is_running (session, error))
    return FALSE;

  /* FIXME: this runs the mainloop; we need to make
   * this async
   */
  if (gsm_query_logout ())
    gsm_session_quit (session);

  return TRUE;
}

static gboolean
gsm_session_dbus_quit (GsmSession  *session,
		       GError     **error)
{
  gsm_debug (SESSION, "Received Quit D-BUS message\n");

  if (!check_is_running (session, error))
    return FALSE;

  gsm_session_quit (session);

  return TRUE;
}

typedef struct
{
  char **service_ids;
  int    i;
} ListifyServicesData;

static void
listify_services (char                *id,
		  GsmService          *service,
		  ListifyServicesData *listify_data)
{
  listify_data->service_ids [listify_data->i++] = g_strdup (id);
}

static gboolean
gsm_session_dbus_list_services (GsmSession   *session,
				char       ***service_ids_ret,
				GError      **error)
{
  ListifyServicesData listify_data;
  int                 n_services;

  gsm_debug (SESSION, "Received ListServices D-BUS message\n");

  if (!check_is_running (session, error))
    return FALSE;

  n_services = g_hash_table_size (session->priv->services);

  listify_data.service_ids = g_new0 (char *, n_services + 1);
  listify_data.i           = 0;

  g_hash_table_foreach (session->priv->services,
			(GHFunc) listify_services,
			&listify_data);

  g_assert (listify_data.i == n_services);

  *service_ids_ret = listify_data.service_ids;

  return TRUE;
}

static GsmService *
lookup_service (GsmSession  *session,
		const char  *service_id,
		GError     **error)
{
  GsmService *service;

  if ((service = g_hash_table_lookup (session->priv->services, service_id)) == NULL)
    {
      g_set_error (error,
		   GSM_SESSION_ERROR,
		   GSM_SESSION_ERROR_SERVICE_NOT_FOUND,
		   _("The service '%s' was not found"),
		   service_id);
      return NULL;
    }

  return service;
}

static gboolean
gsm_session_dbus_start_service (GsmSession  *session,
				const char  *service_id,
				GError     **error)
{
  GsmService *service;

  gsm_debug (SESSION, "Received StartService D-BUS message for service ID '%s'\n",
	     service_id);

  if (!check_is_running (session, error))
    return FALSE;

  if ((service = lookup_service (session, service_id, error)) == NULL)
    return FALSE;

  return gsm_service_start (service, error);
}

static gboolean
gsm_session_dbus_stop_service (GsmSession  *session,
			       const char  *service_id,
			       GError     **error)
{
  GsmService *service;

  gsm_debug (SESSION, "Received StopService D-BUS message for service ID '%s'\n",
	     service_id);

  if (!check_is_running (session, error))
    return FALSE;

  if ((service = lookup_service (session, service_id, error)) == NULL)
    return FALSE;

  return gsm_service_stop (service);
}

static gboolean
gsm_session_dbus_restart_service (GsmSession  *session,
				  const char  *service_id,
				  GError     **error)
{
  GsmService *service;

  gsm_debug (SESSION, "Received RestartService D-BUS message for service ID '%s'\n",
	     service_id);

  if (!check_is_running (session, error))
    return FALSE;

  if ((service = lookup_service (session, service_id, error)) == NULL)
    return FALSE;

  return gsm_service_restart (service, error);
}

static void
gsm_session_notify_service_enabled (GsmSession *session,
				    GParamSpec *pspec,
				    GsmService *service)
{
  gsm_debug (SESSION, "Service '%s' (%p) now %s\n",
	     gsm_service_get_id (service),
	     service,
	     gsm_service_get_enabled (service) ? "enabled" : "disabled");

  if (!session->priv->is_running)
    return;

  if (gsm_service_get_enabled (service))
    {
      GError *error;

      error = NULL;
      if (!gsm_service_start (service, &error))
	{
	  gsm_warning ("Failed to start service '%s': %s",
		       gsm_service_get_id (service),
		       error->message);

	  gsm_service_show_error_dialog (service, error);

	  g_error_free (error);
	}
    }
  else
    {
      gsm_service_stop (service);
    }
}

static gboolean
gsm_session_load_services_dir (GsmSession  *session,
			       const char  *services_dir,
			       GError     **err)
{
  GDir       *dir;
  GError     *error;
  const char *filename;

  g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

  gsm_debug (SESSION, "Loading services dir: %s\n", services_dir);

  error = NULL;
  dir = g_dir_open (services_dir, 0, &error);
  if (dir == NULL)
    {
      gsm_warning ("Failed to open services dir '%s': %s",
		   services_dir, error->message);
      g_propagate_error (err, error);
      return FALSE;
    }

  while ((filename = g_dir_read_name (dir)) != NULL)
    {
      GsmService *service;
      char       *id;
      char       *path;

      if (!g_str_has_suffix (filename, ".desktop-service"))
	continue;

      id = g_path_get_basename (filename);
      id[strlen (id) - strlen (".desktop-service")] = '\0';

      path = g_build_filename (services_dir, filename, NULL);

      if ((service = gsm_service_new (id, path)) != NULL)
	{
	  g_signal_connect_swapped (service, "notify::enabled",
				    G_CALLBACK (gsm_session_notify_service_enabled),
				    session);
	  g_hash_table_replace (session->priv->services,
				(char *) gsm_service_get_id (service),
				service);
	}

      g_free (path);
      g_free (id);
    }

  g_dir_close (dir);

  return TRUE;
}

static void
gsm_session_handle_failed_services_response (GsmSession *session,
					     int         response_id,
					     GtkWidget  *dialog)
{
  switch (response_id)
    {
    case GSM_RESPONSE_LOGOUT:
      if (session->priv->is_running)
	gsm_session_quit (session);
      break;

    case GTK_RESPONSE_CLOSE:
    default:
      break;
    }

  gtk_widget_destroy (dialog);
}

static GSList *
gsm_session_start_services (GsmSession *session,
			    GSList     *services,
			    GCallback   completed_callback)
{
  GSList *l;
  GSList *pending_services;

  pending_services = NULL;

  for (l = services; l; l = l->next)
    {
      GsmService *service = l->data;
      GError     *error;

      if (!gsm_service_get_enabled (service))
	{
	  gsm_debug (SESSION, "Not starting service '%s' (%p) because it isn't enabled\n",
		     gsm_service_get_id (service), service);
	  continue;
	}

      error = NULL;
      if (!gsm_service_start (service, &error))
	{
	  GsmFailedService *loser;

	  loser = gsm_failed_service_new (gsm_service_get_name (service),
					  error->message);

	  session->priv->failed_services =
	    g_slist_append (session->priv->failed_services, loser);

	  g_error_free (error);
	  continue;
	}

      gsm_debug (SESSION, "Waiting for service '%s' (%p) to complete startup\n",
		 gsm_service_get_id (service), service);

      g_signal_connect_swapped (service, "startup-completed",
				completed_callback, session);

      pending_services = g_slist_append (pending_services, service);
    }

  return pending_services;
}

typedef struct
{
  GSList *other_services;
  GSList *core_service_ids;
} ListifyOtherServicesData;

static void
listify_other_services (const char               *id,
			GsmService               *service,
			ListifyOtherServicesData *listify_data)
{
  GSList *l;

  for (l = listify_data->core_service_ids; l != NULL; l = l->next)
    {
      const char *core_service_id = l->data;

      if (!strcmp (core_service_id, id))
	return;
    }

  listify_data->other_services =
    g_slist_append (listify_data->other_services, service);
}

static GSList *
gsm_session_get_other_services (GsmSession *session)
{
  ListifyOtherServicesData listify_data;

  listify_data.other_services = NULL;
  listify_data.core_service_ids = gsm_config_get_core_services (session->priv->config);
  g_hash_table_foreach (session->priv->services,
			(GHFunc) listify_other_services,
			&listify_data);

  return listify_data.other_services;
}

static void gsm_session_other_service_completed (GsmSession *session,
						 GsmService *service);
static void
disconnect_pending_other_service (GsmService *service,
				  GsmSession *session)
{
  g_signal_handlers_disconnect_by_func (service,
					G_CALLBACK (gsm_session_other_service_completed),
					session);
}

static void
gsm_session_other_service_completed (GsmSession *session,
				     GsmService *service)
{
  if (service != NULL)
    {
      gsm_debug (SESSION, "Other service '%s' (%p) startup completed\n",
		 gsm_service_get_id (service), service);

      disconnect_pending_other_service (service, session);

      session->priv->pending_other_services =
	g_slist_remove (session->priv->pending_other_services,
			service);
    }

  if (session->priv->pending_other_services == NULL)
    {
      gsm_debug (SESSION, "Completed startup of all other services\n");

      if (session->priv->failed_services != NULL)
	{
	  gsm_debug (SESSION, "Showing failed services dialog with %d losers\n",
		     g_slist_length (session->priv->failed_services));

	  session->priv->failed_services_dialog =
	    gsm_show_failed_services_dialog (session->priv->failed_services);

	  g_signal_connect_swapped (session->priv->failed_services_dialog, "response",
				    G_CALLBACK (gsm_session_handle_failed_services_response),
				    session);

	  g_object_add_weak_pointer (G_OBJECT (session->priv->failed_services_dialog),
				     (gpointer *) &session->priv->failed_services_dialog);

	  g_slist_foreach (session->priv->failed_services,
			   (GFunc) gsm_failed_service_free,
			   NULL);
	  g_slist_free (session->priv->failed_services);
	  session->priv->failed_services = NULL;
	}
    }
  else
    {
      gsm_debug (SESSION, "Waiting on %d other services\n",
		 g_slist_length (session->priv->pending_other_services));
    }
}

static void
gsm_session_start_other_services (GsmSession *session)
{
  GSList *other_services;

  other_services = gsm_session_get_other_services (session);

  session->priv->pending_other_services =
    gsm_session_start_services (session,
				other_services,
				G_CALLBACK (gsm_session_other_service_completed));

  gsm_session_other_service_completed (session, NULL);
}

static void gsm_session_core_service_completed (GsmSession *session,
						GsmService *service);

static void
disconnect_pending_core_service (GsmService *service,
				 GsmSession *session)
{
  g_signal_handlers_disconnect_by_func (service,
					G_CALLBACK (gsm_session_core_service_completed),
					session);
}

static void
gsm_session_core_service_completed (GsmSession *session,
				    GsmService *service)
{
  if (service != NULL)
    {
      gsm_debug (SESSION, "Core service '%s' (%p) startup completed\n",
		 gsm_service_get_id (service), service);

      disconnect_pending_core_service (service, session);

      session->priv->pending_core_services =
	g_slist_remove (session->priv->pending_core_services,
			service);
    }

  if (session->priv->pending_core_services == NULL)
    {
      gsm_debug (SESSION, "Completed startup of all core services\n");

      gsm_session_start_other_services (session);
    }
  else
    {
      gsm_debug (SESSION, "Waiting on %d core services\n",
		 g_slist_length (session->priv->pending_core_services));
    }
}

static void
gsm_session_start_core_services (GsmSession *session)
{
  GSList *l;
  GSList *core_service_ids;
  GSList *core_services;

  core_services = NULL;
  core_service_ids = gsm_config_get_core_services (session->priv->config);

  for (l = core_service_ids; l; l = l->next)
    {
      const char *id = l->data;
      GsmService *service;

      if ((service = g_hash_table_lookup (session->priv->services, id)) == NULL)
	{
	  GsmFailedService *loser;
	  char             *error_text;

	  gsm_warning ("Could not locate .desktop-service file for core service '%s'", id);

	  error_text = g_strdup_printf (_("No .desktop-service file found for core service '%s'"), id);

	  loser = gsm_failed_service_new (id, error_text);

	  session->priv->failed_services =
	    g_slist_append (session->priv->failed_services, loser);

	  g_free (error_text);

	  continue;
	}

      core_services = g_slist_append (core_services, service);
    }

  session->priv->pending_core_services = 
    gsm_session_start_services (session,
				core_services,
				G_CALLBACK (gsm_session_core_service_completed));

  gsm_session_core_service_completed (session, NULL);
}

gboolean
gsm_session_run (GsmSession *session)
{
  GError *error;
  GSList *l;

  g_return_val_if_fail (session != NULL, FALSE);
  g_return_val_if_fail (!session->priv->is_running, FALSE);

  gsm_debug (SESSION, "Starting session %p\n", session);

  error = NULL;
  if (!(session->priv->config = gsm_config_parse (GSM_CONFIG_PATH, &error)))
    {
      char *secondary_text;

      secondary_text = g_strdup_printf (_("Failed to read '%s': %s"),
					GSM_CONFIG_PATH,
					error->message);
      gsm_run_login_failed_dialog (_("Failed to read GNOME session manager configuration."),
				   secondary_text);
      g_free (secondary_text);

      g_error_free (error);

      return FALSE;
    }

  session->priv->services = g_hash_table_new_full (g_str_hash,
						   g_str_equal,
						   NULL,
						   (GDestroyNotify) g_object_unref);

  for (l = gsm_config_get_services_dirs (session->priv->config); l; l = l->next)
    {
      const char *services_dir = l->data;

      error = NULL;
      if (!gsm_session_load_services_dir (session, services_dir, &error))
	{
	  char *secondary_text;

	  secondary_text = g_strdup_printf (_("Failed to load .desktop-service files from '%s': %s"),
					    services_dir, error->message);
	  gsm_run_login_failed_dialog (_("Failed to read GNOME session manager configuration."),
				       secondary_text);
	  g_free (secondary_text);

	  g_error_free (error);

	  g_hash_table_destroy (session->priv->services);
	  session->priv->services = NULL;

	  gsm_config_free (session->priv->config);
	  session->priv->config = NULL;

	  return FALSE;
	}
    }

  gsm_session_start_core_services (session);

  session->priv->is_running = TRUE;

  gsm_debug (SESSION, "Entering main loop\n");

  gtk_main ();

  gsm_debug (SESSION, "Exited main loop\n");

  gsm_session_stop (session);

  session->priv->is_running = FALSE;

  return TRUE;
}

void
gsm_session_stop (GsmSession *session)
{
  gsm_debug (SESSION, "Stopping session %p\n", session);

  if (session->priv->failed_services_dialog != NULL)
    gtk_widget_destroy (session->priv->failed_services_dialog);
  session->priv->failed_services_dialog = NULL;

  if (session->priv->failed_services != NULL)
    {
      g_slist_foreach (session->priv->failed_services,
		       (GFunc) gsm_failed_service_free,
		       NULL);
      g_slist_free (session->priv->failed_services);
      session->priv->failed_services = NULL;
    }

  if (session->priv->pending_other_services != NULL)
    {
      g_slist_foreach (session->priv->pending_other_services,
		       (GFunc) disconnect_pending_other_service,
		       session);
      g_slist_free (session->priv->pending_other_services);
      session->priv->pending_other_services = NULL;
    }

  if (session->priv->pending_core_services != NULL)
    {
      g_slist_foreach (session->priv->pending_core_services,
		       (GFunc) disconnect_pending_core_service,
		       session);
      g_slist_free (session->priv->pending_core_services);
      session->priv->pending_core_services = NULL;
    }

  if (session->priv->services)
    g_hash_table_destroy (session->priv->services);
  session->priv->services = NULL;

  if (session->priv->config != NULL)
    gsm_config_free (session->priv->config);
  session->priv->config = NULL;
}

void
gsm_session_quit (GsmSession *session)
{
  g_return_if_fail (session->priv->is_running);

  gsm_debug (SESSION, "Ending session %p\n", session);

  gtk_main_quit ();
}
