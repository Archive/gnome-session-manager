/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GSM_SESSION_H__
#define __GSM_SESSION_H__

#include <glib-object.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#define GSM_TYPE_SESSION         (gsm_session_get_type ())
#define GSM_SESSION(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GSM_TYPE_SESSION, GsmSession))
#define GSM_SESSION_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GSM_TYPE_SESSION, GsmSessionClass))
#define GSM_IS_SESSION(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GSM_TYPE_SESSION))
#define GSM_IS_SESSION_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GSM_TYPE_SESSION))
#define GSM_SESSION_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GSM_TYPE_SESSION, GsmSessionClass))

#define GSM_SESSION_ERROR (g_quark_from_static_string ("gsm-session-error-quark"))

typedef enum
{
  GSM_SESSION_ERROR_NOT_RUNNING,
  GSM_SESSION_ERROR_SERVICE_NOT_FOUND
} GsmSessionError;

typedef struct _GsmSession        GsmSession;
typedef struct _GsmSessionClass   GsmSessionClass;
typedef struct _GsmSessionPrivate GsmSessionPrivate;

struct _GsmSession
{
  GObject object;

  GsmSessionPrivate *priv;
};

struct _GsmSessionClass
{
  GObjectClass object_class;
};

GType gsm_session_get_type (void) G_GNUC_CONST;

GsmSession *gsm_session_get  (void);

gboolean    gsm_session_run  (GsmSession *session);
void        gsm_session_stop (GsmSession *session);
void        gsm_session_quit (GsmSession *session);

G_END_DECLS

#endif /* __GSM_SESSION_H__ */
