/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>

#include "gsm-startup-monitor.h"

#include <gdk/gdk.h>
#include <gdk/gdkx.h>

#include "gsm-debug.h"

struct _GsmStartupMonitorPrivate
{
  SnDisplay         *sn_display;
  SnMonitorContext **sn_contexts;
};

enum
{
  STARTUP_COMPLETED,
  LAST_SIGNAL
};

static GdkFilterReturn gsm_startup_monitor_handle_x_event (GdkXEvent         *gdkxevent,
							   GdkEvent          *event,
							   GsmStartupMonitor *monitor);

#define GSM_STARTUP_MONITOR_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj),                     \
									   GSM_TYPE_STARTUP_MONITOR,  \
									   GsmStartupMonitorPrivate))

G_DEFINE_TYPE (GsmStartupMonitor, gsm_startup_monitor, G_TYPE_OBJECT);

static guint signals[LAST_SIGNAL] = { 0 };

static void
gsm_startup_monitor_finalize (GObject *object)
{
  GsmStartupMonitor *monitor = GSM_STARTUP_MONITOR (object);

  gsm_debug (STARTUP_MONITOR, "Finalizing startup monitor %p\n", monitor);

  gdk_window_remove_filter (NULL,
			    (GdkFilterFunc) gsm_startup_monitor_handle_x_event,
			    monitor);

  if (monitor->priv->sn_contexts != NULL)
    {
      int i;

      for (i = 0; monitor->priv->sn_contexts[i] != NULL; i++)
	{
	  sn_monitor_context_unref (monitor->priv->sn_contexts[i]);
	  monitor->priv->sn_contexts[i] = NULL;
	}

      g_free (monitor->priv->sn_contexts);
      monitor->priv->sn_contexts = NULL;
    }

  if (monitor->priv->sn_display != NULL)
    sn_display_unref (monitor->priv->sn_display);
  monitor->priv->sn_display = NULL;

  monitor->priv = NULL;

  G_OBJECT_CLASS (gsm_startup_monitor_parent_class)->finalize (object);
}

static void
gsm_startup_monitor_class_init (GsmStartupMonitorClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  gobject_class->finalize = gsm_startup_monitor_finalize;

  signals[STARTUP_COMPLETED] =
      g_signal_new ("startup-completed",
		    G_OBJECT_CLASS_TYPE (gobject_class),
		    G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
		    G_STRUCT_OFFSET (GsmStartupMonitorClass, startup_completed),
		    NULL, NULL,
		    g_cclosure_marshal_VOID__VOID,
		    G_TYPE_NONE, 0);

  g_type_class_add_private (gobject_class, sizeof (GsmStartupMonitorPrivate));
}

static void
gsm_startup_monitor_push_sn_error_trap (SnDisplay *display,
					Display   *xdisplay)
{
  gdk_error_trap_push ();
}

static void
gsm_startup_monitor_pop_sn_error_trap (SnDisplay *display,
				       Display   *xdisplay)
{
  gdk_error_trap_pop ();
}

static void
gsm_startup_monitor_handle_sn_event (SnMonitorEvent    *event,
				     GsmStartupMonitor *monitor)
{
  SnStartupSequence *sequence;
  const char        *id;

  if (sn_monitor_event_get_type (event) != SN_MONITOR_EVENT_COMPLETED)
    return;

  sequence = sn_monitor_event_get_startup_sequence (event);

  id = sn_startup_sequence_get_id (sequence);

  gsm_debug (STARTUP_MONITOR, "Got completed event for '%s'\n", id);

  g_signal_emit (monitor,
		 signals[STARTUP_COMPLETED],
		 g_quark_from_string (id));
}

static GdkFilterReturn
gsm_startup_monitor_handle_x_event (GdkXEvent         *gdkxevent,
				    GdkEvent          *event,
				    GsmStartupMonitor *monitor)
{
  XEvent *xevent = gdkxevent;

  if (xevent->type == ClientMessage)
    {
      sn_display_process_event (monitor->priv->sn_display,
				xevent);
    }

  return GDK_FILTER_CONTINUE;
}

static void
gsm_startup_monitor_init (GsmStartupMonitor *monitor)
{
  GdkDisplay *display;
  Display    *xdisplay;
  int         n_screens;
  int         i;

  monitor->priv = GSM_STARTUP_MONITOR_GET_PRIVATE (monitor);

  display  = gdk_display_get_default ();
  xdisplay = gdk_x11_display_get_xdisplay (display);

  monitor->priv->sn_display = sn_display_new (xdisplay,
					      gsm_startup_monitor_push_sn_error_trap,
					      gsm_startup_monitor_pop_sn_error_trap);

  n_screens = gdk_display_get_n_screens (display);

  monitor->priv->sn_contexts = g_new0 (SnMonitorContext *, n_screens + 1);

  for (i = 0; i < n_screens; i++)
    {
      GdkScreen *screen;
      GdkWindow *root_window;

      screen      = gdk_display_get_screen (display, i);
      root_window = gdk_screen_get_root_window (screen);

      gdk_window_set_events (root_window,
			     gdk_window_get_events (root_window) | GDK_PROPERTY_CHANGE_MASK);

      monitor->priv->sn_contexts[i] =
	sn_monitor_context_new (monitor->priv->sn_display,
				i,
				(SnMonitorEventFunc) gsm_startup_monitor_handle_sn_event,
				monitor,
				NULL);
    }

  gdk_window_add_filter (NULL,
			 (GdkFilterFunc) gsm_startup_monitor_handle_x_event,
			 monitor);
}

static void
gsm_startup_monitor_nullify_singleton (GsmStartupMonitor **singleton)
{
  g_return_if_fail (singleton != NULL);

  *singleton = NULL;
}

GsmStartupMonitor *
gsm_startup_monitor_get (void)
{
  static GsmStartupMonitor *singleton = NULL;

  if (singleton == NULL)
    {
      gsm_debug (STARTUP_MONITOR, "Creating GsmStartupMonitor singleton\n");

      singleton = g_object_new (GSM_TYPE_STARTUP_MONITOR, NULL);
      g_object_weak_ref (G_OBJECT (singleton),
			 (GWeakNotify) gsm_startup_monitor_nullify_singleton,
			 &singleton);
    }
  else
    {
      singleton = g_object_ref (singleton);
    }

  return singleton;
}

SnDisplay *
gsm_startup_monitor_get_sn_display (GsmStartupMonitor *monitor)
{
  g_return_val_if_fail (monitor != NULL, NULL);

  return monitor->priv->sn_display;
}
