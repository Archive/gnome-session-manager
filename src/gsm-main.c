/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "gsm-session.h"
#include "gsm-gconf.h"
#include "gsm-debug.h"

int
main (int argc, char **argv)
{
  GsmSession *session;
  int         retval;

  bindtextdomain (GETTEXT_PACKAGE, GSM_LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  gtk_init (&argc, &argv);

  gsm_setup_debug_flags ();

  if (!gsm_gconf_run_sanity_check ())
    return 1;

  if (!(session = gsm_session_get ()))
    return 1;

  retval = gsm_session_run (session) ? 0 : 1;

  g_object_unref (session);

  gsm_gconf_shutdown ();

  return retval;
}
