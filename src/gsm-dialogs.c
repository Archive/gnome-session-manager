/*
 * Copyright (C) 2005 Red Hat, Inc.
 * Copyright (C) 2001 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 *      Havoc Pennington <hp@redhat.com>
 */

#include <config.h>

#include "gsm-dialogs.h"

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "gsm-service.h"

void
gsm_run_login_failed_dialog (const char *primary_text,
			     const char *secondary_text)
{
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new (NULL,
				   0,
				   GTK_MESSAGE_ERROR,
				   GTK_BUTTONS_NONE,
				   "%s",
				   primary_text);

  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "%s", secondary_text);

  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Abort login"), GTK_RESPONSE_CLOSE);

  gtk_dialog_run (GTK_DIALOG (dialog));

  gtk_widget_destroy (dialog);
}

GtkWidget *
gsm_show_too_many_respawns_dialog (const char *service_name)
{
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new (NULL,
				   0,
				   GTK_MESSAGE_ERROR,
				   GTK_BUTTONS_NONE,
				   _("Error re-starting service '%s'."),
				   service_name);

  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
					    _("The service '%s' has been re-started several times in the last few seconds, but crashed or exited each time. What do you want to do with this service?"),
					    service_name);

  gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			  _("_Log out"),         GSM_RESPONSE_LOGOUT,
			  _("_Keep restarting"), GSM_RESPONSE_KEEP_TRYING,
			  _("_Stop restarting"), GSM_RESPONSE_DISABLE,
			  NULL);

  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GSM_RESPONSE_DISABLE);

  gtk_widget_show (dialog);

  return dialog;
}

struct _GsmFailedService
{
  char *name;
  char *error;
};

GsmFailedService *
gsm_failed_service_new (const char *service_name,
			const char *error_text)
{
  GsmFailedService *loser;

  g_return_val_if_fail (service_name != NULL, NULL);
  g_return_val_if_fail (error_text != NULL, NULL);

  loser = g_new0 (GsmFailedService, 1);

  loser->name  = g_strdup (service_name);
  loser->error = g_strdup (error_text);

  return loser;
}

void
gsm_failed_service_free (GsmFailedService *loser)
{
  g_return_if_fail (loser != NULL);

  g_free (loser->name);
  loser->name = NULL;

  g_free (loser->error);
  loser->error = NULL;

  g_free (loser);
}

GtkWidget *
gsm_show_failed_service_dialog (GsmFailedService *loser)
{
  GtkWidget *dialog;

  g_return_val_if_fail (loser != NULL, NULL);

  dialog = gtk_message_dialog_new (NULL,
				   0,
				   GTK_MESSAGE_ERROR,
				   GTK_BUTTONS_CLOSE,
				   _("The desktop service '%s' could not be started"),
				   loser->name);

  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
					    "%s",
					    loser->error);

  g_signal_connect (dialog, "response",
		    G_CALLBACK (gtk_widget_destroy), NULL);

  gtk_widget_show (dialog);

  return dialog;
}

enum
{
  COLUMN_NAME,
  COLUMN_ERROR,
  COLUMN_LAST
};

GtkWidget *
gsm_show_failed_services_dialog (GSList *failed_services)
{
  GtkWidget         *dialog;
  GtkWidget         *button;
  GtkWidget         *scrolled;
  GtkWidget         *tree_view;
  GtkListStore      *model;
  GtkCellRenderer   *cell;
  GtkTreeViewColumn *column;
  GSList            *l;

  g_return_val_if_fail (failed_services != NULL, NULL);

  dialog = gtk_message_dialog_new (NULL,
				   0,
				   GTK_MESSAGE_ERROR,
				   GTK_BUTTONS_NONE,
				   _("Some desktop services failed to start."));

  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
					    _("There were errors starting some important parts of the desktop environment:")); 

  button = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Log out"),    GSM_RESPONSE_LOGOUT);
  button = gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);

  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);
  gtk_widget_grab_focus (button);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
		      scrolled,
		      TRUE,
		      TRUE,
		      0);
  gtk_widget_show (scrolled);

  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled), 3);

  gtk_window_set_geometry_hints (GTK_WINDOW (dialog), scrolled, NULL, 0);
  gtk_window_set_resizable (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_default_size (GTK_WINDOW (dialog), 400, 225);

  model = gtk_list_store_new (COLUMN_LAST,
			      G_TYPE_STRING,
			      G_TYPE_STRING);

  tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
  gtk_container_add (GTK_CONTAINER (scrolled), tree_view);
  gtk_widget_show (tree_view);

  gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view)),
                               GTK_SELECTION_NONE);

  for (l = failed_services; l != NULL; l = l->next)
    {
      GsmFailedService *loser = l->data;
      GtkTreeIter       iter;

      gtk_list_store_append (model, &iter);
      gtk_list_store_set (model, &iter,
			  COLUMN_NAME,  loser->name,
			  COLUMN_ERROR, loser->error,
			  -1);
    }

  cell = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Service"),
						     cell,
						     "text", COLUMN_NAME,
						     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), column);

  cell = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Error Details"),
						     cell,
						     "text", COLUMN_ERROR,
						     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), column);

  g_object_unref (model);

  gtk_widget_show (dialog);

  return dialog;
}
