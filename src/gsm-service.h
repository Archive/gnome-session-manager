/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GSM_SERVICE_H__
#define __GSM_SERVICE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GSM_TYPE_SERVICE         (gsm_service_get_type ())
#define GSM_SERVICE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GSM_TYPE_SERVICE, GsmService))
#define GSM_SERVICE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GSM_TYPE_SERVICE, GsmServiceClass))
#define GSM_IS_SERVICE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GSM_TYPE_SERVICE))
#define GSM_IS_SERVICE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GSM_TYPE_SERVICE))
#define GSM_SERVICE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GSM_TYPE_SERVICE, GsmServiceClass))

typedef struct _GsmService        GsmService;
typedef struct _GsmServiceClass   GsmServiceClass;
typedef struct _GsmServicePrivate GsmServicePrivate;

struct _GsmService
{
  GObject object;

  GsmServicePrivate *priv;
};

struct _GsmServiceClass
{
  GObjectClass object_class;

  void (* startup_completed) (GsmService *service);
};

GType       gsm_service_get_type        (void) G_GNUC_CONST;

GsmService *gsm_service_new               (const char  *id,
					   const char  *path);
						      
const char *gsm_service_get_id            (GsmService  *service);
void        gsm_service_set_id            (GsmService  *service,
					   const char  *id);
const char *gsm_service_get_path          (GsmService  *service);
void        gsm_service_set_path          (GsmService  *service,
					   const char  *path);
const char *gsm_service_get_name          (GsmService  *service);
void        gsm_service_set_name          (GsmService  *service,
					   const char  *name);
const char *gsm_service_get_description   (GsmService  *service);
void        gsm_service_set_description   (GsmService  *service,
					   const char  *description);
const char *gsm_service_get_icon_name     (GsmService  *service);
void        gsm_service_set_icon_name     (GsmService  *service,
					   const char  *icon_name);
const char *gsm_service_get_exec          (GsmService  *service);
void        gsm_service_set_exec          (GsmService  *service,
					   const char  *exec);
gboolean    gsm_service_get_enabled       (GsmService  *service);
const char *gsm_service_get_enabled_key   (GsmService  *service);
void        gsm_service_set_enabled_key   (GsmService  *service,
					   const char  *enabled_key);
const char *gsm_service_get_disabled_key  (GsmService  *service);
void        gsm_service_set_disabled_key  (GsmService  *service,
					   const char  *disabled_key);
						      
gboolean    gsm_service_start             (GsmService  *service,
					   GError     **err);
gboolean    gsm_service_restart           (GsmService  *service,
					   GError     **err);
gboolean    gsm_service_stop              (GsmService  *service);

void        gsm_service_show_error_dialog (GsmService  *service,
					   GError      *error);

G_END_DECLS

#endif /* __GSM_SERVICE_H__ */
